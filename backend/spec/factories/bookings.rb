# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :booking do
    customer_name "Pedro Paramo"
    phone "+372 1234 4321"
    pickup_address "Vanemuise 6, Tartu, Estonia"
    pickup_time Time.now
    status nil
  end
end
