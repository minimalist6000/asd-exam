require 'spec_helper'

describe TaxiAllocatorJob do

  it 'should immediately execute a booking assignment' do
    booking = create(:booking)
    TaxiAllocatorJob.new.async.perform(booking.id)
    expect( Booking.find(booking.id).status ).to eql('FORWARDED')
  end

  it 'should eventually execute a deferred booking assignment' do
    booking = create(:booking)
    TaxiAllocatorJob.new.async.perform_later(5, booking.id)
    expect( Booking.find(booking.id).status ).to eql(nil)
    sleep 7
    expect( Booking.find(booking.id).status ).to eql('FORWARDED')
  end

  it 'should select one taxi' do
    taxi1 = create(:bus_station)
    taxi2 = create(:hospital)
    booking = create(:booking) # It is assumed I am in front PlayTech building

    json = TaxiAllocatorJob.new.select_taxi(booking)

    expect(Booking.find(booking.id).taxi).to eql(taxi1)
  end

  it 'should be able to calculate the cost of the fare with day rate' do
    # day rate
    expectedCost = 4.20 + 5.68
    time = "2014-06-05 11:58:14.603164"
    duration_in_sec = "600"
    cost = TaxiAllocatorJob.calculateCost(time, duration_in_sec)
    expect(cost).to eq(expectedCost.round(2))
  end


  it 'should be able to calculate the cost of the fare with night rate' do
    # night rate
    expectedCost = 5.20 + 6.31
    time = "2014-06-05 19:58:14.603164"
    duration_in_sec = "600"
    cost = TaxiAllocatorJob.calculateCost(time, duration_in_sec)
    expect(cost).to eq(expectedCost.round(2))
  end


  it 'should be able to calculate the cost of the fare with peak rate' do
    # night rate
    expectedCost = 6.50 + 9.15
    time = "2014-06-07 22:58:14.603164"
    duration_in_sec = "600"
    cost = TaxiAllocatorJob.calculateCost(time, duration_in_sec)
    expect(cost).to eq(expectedCost.round(2))
  end

  # it 'should calculate the estimated cost of the fare' do
  #   # taxi1 = create(:bus_station)
  #   # booking = create(:booking) # It is assumed I am in front PlayTech building
  #   #
  #   # json = TaxiAllocatorJob.new.select_taxi(booking)
  #   #
  #   # expect(Booking.find(booking.id).taxi).to eql(taxi1)
  # end
end