Feature: Taxi booking with fare calculation
  As a customer
  So that I can have a taxi picking me up at a given address
  I want to book the taxi via STRS and I want to know how much will the ride cost

  @javascript
  Scenario: Show fare to customer
    Given I have access to STRS web application
      And a Taxi drivers are waiting for ride requests
      And I enter my current address, destination and contact information
      And I submit my booking request
     When one taxi driver accepts the request
     Then I should be notified that my request is being processed
      And I should eventually receive a booking confirmation with the estimate of the ride cost.