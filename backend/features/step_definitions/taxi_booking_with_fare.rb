Given(/^I have access to STRS web application$/) do
  FactoryGirl.create(:bus_station)
  @customer = Capybara::Session.new(:selenium)
  @customer.visit 'http://localhost:9000/#/'
end

Given(/^a Taxi drivers are waiting for ride requests$/) do
  @taxi_driver = Capybara::Session.new(:selenium)
  @taxi_driver.visit 'http://localhost:9090/#/'
end

Given(/^I enter my current address, destination and contact information$/) do
  @customer.fill_in('Name', with: 'Juan Perez')
  @customer.fill_in('Pickup Address', with: 'Liivi 2')
  @customer.fill_in('Destination Address', with: 'Pikk 10')
  @customer.fill_in('Phone', with: '+372 1234 4321')
end

Given(/^I submit my booking request$/) do
  @customer.click_button('Submit')
end

When(/^one taxi driver accepts the request$/) do
  @taxi_driver.click_button('Accept')
end

Then(/^I should be notified that my request is being processed$/) do
  expect(@customer.find_by_id('sync_notification').text).to eq('Booking is being processed')
end

Then(/^I should eventually receive a booking confirmation with the estimate of the ride cost\.$/) do
  sleep 3
  puts @customer.find_by_id('async_notification').text
  asyncNotification = @customer.find_by_id('async_notification').text
  puts asyncNotification

  expect(asyncNotification).to match(/Your taxi will arrive in: /)
  expect(asyncNotification).to match(/Estimated fare is:/)

  # expect(@customer.find_by_id('async_notification').text).to match(/Your taxi will arrive in/)
end