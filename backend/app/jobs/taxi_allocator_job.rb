class TaxiAllocatorJob
  include SuckerPunch::Job

  def perform(booking_id)
    booking = Booking.find(booking_id)
    booking.taxi = nil

    puts "ZZZZ"
    puts "preSelectTaxi"
    select_taxi(booking)
    puts "afterSelectTaxi"
    booking = Booking.find(booking_id)
    # puts booking.to_json

    Pusher.trigger('strs','booking_request', booking.to_json)
    Pusher.logger.info 'SENT VIA PUSHER'
    booking.update_attributes status: 'FORWARDED'
  end

  def perform_later(sec, booking_id)
    after(sec) {
      perform(booking_id)
    }
  end

  def compute_distance(addr1, addr2)
    addr1 << ', Tartu, Estonia'
    addr2 << ', Tartu, Estonia'


    departure_Unix_stamp = Time.now.to_i
    #url = "http://maps.googleapis.com/maps/api/directions/json?origin=#{ CGI.escape( addr1 ) }&destination=#{ CGI.escape( addr2 ) }&sensor=false&mode=driving"
    url = "http://maps.googleapis.com/maps/api/directions/json?origin=#{ CGI.escape( addr1 ) }&destination=#{ CGI.escape( addr2 ) }&departure_time=#{departure_Unix_stamp }&sensor=false&mode=driving"

    puts "URL: #{url}"

    resp = JSON.parse( RestClient.get(url) )
    distance = resp['routes'][0]['legs'].map {|m| m['distance']['value']}.reduce(:+)
    duration = resp['routes'][0]['legs'].map {|m| m['duration']['value']}.reduce(:+)
    return distance, duration
  end

  def select_taxi(booking)
    rejections = BookingRejection.where(:booking_id => booking.id).map { |rj| rj.taxi_id }

    taxis_hash = {}
    Taxi.find(:all).each do |taxi|
      unless rejections.include?(taxi.id)
        distance, duration = compute_distance(booking.pickup_address, taxi.current_address)
        ride = Hash["taxi" => taxi, "distance" => distance, "duration" => duration]
        # puts "xxxxxxx"
        # puts "#{ride}"
        taxis_hash[distance] = ride
      end
    end

    taxis_hash.sort.map do |distance, ride|
      booking.taxi = ride["taxi"]
      booking.distance = ride["distance"]
      booking.duration = ride["duration"]

      puts "XXXXXX"
      # puts ride["distance"]
      # puts ride["duration"]
      puts "pickup time: #{booking.pickup_time}"
      puts "created_at: #{booking.created_at}"
      puts "duration: #{booking.duration}"

      booking.estimated_cost = TaxiAllocatorJob.calculateCost(booking.created_at, booking.duration)
      booking.save
      break
    end
  end



  def self.calculateCost(date, duration_in_sec)
    puts "calculateCost"
    puts "date: #{date}"
    puts "duration_in_sec: #{duration_in_sec}"

    duration_in_minutes = Integer(duration_in_sec) / 60
    minute_rate, starting_cost = getRateByDate(date)
    cost = starting_cost + (duration_in_minutes * minute_rate)

    puts "cost: #{cost}"
    return cost.round(2)
  end

  def self.getRateByDate(date)
    puts "getRateByDate"

    if date.class == String then
      time = Time.parse date
    else
      time = date
    end

    puts "time: #{time}"
    # time = Time.parse date
    hour = time.hour
    wday = time.wday
    isWeekend = wday == 6 || wday ==0 ? true : false

    puts "hour: #{hour}"
    puts "wDay: #{wday}"
    puts "isWeekend: #{isWeekend}"

    if isWeekend == true and (hour >= 22 or hour < 5)
      minute_rate = 0.915
      starting_cost = 6.50
      # puts "0.915"

    elsif hour >= 9 and hour < 18
      minute_rate = 0.568
      starting_cost = 4.20
      # puts "0.568"

    else
      minute_rate = 0.631
      starting_cost = 5.20
      # puts "0.631"
    end

    puts "minute rate: #{minute_rate}, starting cost: #{starting_cost}"
    return minute_rate, starting_cost
  end
end