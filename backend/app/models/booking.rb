class Booking < ActiveRecord::Base
  attr_accessible :customer_name, :phone, :pickup_address, :pickup_time, :status, :destination_address, :duration, :distance, :estimated_cost

  belongs_to :taxi
end
