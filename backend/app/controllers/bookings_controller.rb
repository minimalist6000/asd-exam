class BookingsController < ApplicationController
  def create
    b = Booking.create(params[:booking])
    b.pickup_time = b.created_at
    b.save
    TaxiAllocatorJob.new.async.perform(b.id)
    render :json => {:message => "Booking is being processed"}, :status => :created
  end


  def updateTaxiAssignment
    puts "updateTaxiAssignment"
    b = params[:booking]
    puts "#{b}"

    booking = Booking.find(b[:id])
    booking.update_attributes status: b[:status]
    puts "#{booking}"

    if (booking.status == "ACCEPTED") then
      duration_in_minutes = Integer(booking.duration) / 60
      duration_in_minutes.round(0)
      message = "Your taxi will arrive in: #{duration_in_minutes} minutes. Estimated fare is: #{booking.estimated_cost} EUR"
    else
      message = "No taxis are available."
    end

    puts message
    Pusher['taxiClient'].trigger('asyncNotification', {:message => message})

    # debugger
  end
end