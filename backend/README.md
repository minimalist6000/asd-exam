# Setting up the project

Of course, you should start by installing all the required gems. Do this by means of the usual command:

```sh
bundle install
```

Please note that I updated rails to its version `3.1.18`, because rails 3.1.17 is not longer available for Windows users.


```sh
rake db:migrate
```

Do not forget to properly configure `Pusher`. To this end, edit the file `config/initializers/pusher.rb`.