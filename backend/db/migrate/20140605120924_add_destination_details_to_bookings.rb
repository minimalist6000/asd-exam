class AddDestinationDetailsToBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :duration, :string
    add_column :bookings, :distance, :string
    add_column :bookings, :estimated_cost, :decimal, :precision => 5, :scale => 2
  end
end
