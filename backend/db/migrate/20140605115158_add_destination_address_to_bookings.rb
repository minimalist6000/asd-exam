class AddDestinationAddressToBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :destination_address, :string
  end
end
