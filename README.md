# Acceptance tests

You should first complete the setup of the three application components (i.e., TaxiClient, TaxiHome, backend).


As the acceptance test involves not only the backend but also two front-end applications (both written in AngularJS) 
you will be required to download `chromedriver`. Do not forget to properly set the `path` variable so as to let 
`cucumber` find `chromedriver`.

In three different terminals you should run the three applications:

```sh
TaxiClient$ grunt serve      # Note that TaxiClient runs in port 9000 

TaxiHome$ grunt serve        # In turn, TaxiHome runs in port 9090

backend$ rails server -e test  # Note that the backend has to be run in a "test" environment
```

In a separate terminal, you can run `cucumber` as usual:

```sh
backend$ cucumber
```

# Unit tests

The project skeleton contains unit test specs for "TaxiClient" and "backend". Play with these tests as you will be 
required to add other similar test cases.