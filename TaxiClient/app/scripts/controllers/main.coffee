'use strict'

app = angular.module('taxiClientApp')
app.controller 'MainCtrl', ($scope, BookingsSyncService, BookingsPusherService) ->
  $scope.name = ''
  $scope.phone = ''
  $scope.pickupAddress = ''
  $scope.destinationAddress = ''
  $scope.syncNotification = ''
  $scope.asyncNotification = ''
  $scope.submit = ->
    BookingsSyncService.save {
      'customer_name': $scope.name
      'phone': $scope.phone
      'pickup_address': $scope.pickupAddress
      'destination_address': $scope.destinationAddress
    }, (data) ->
      $scope.syncNotification = data

  BookingsPusherService.onMessage (data) ->
    $scope.asyncNotification = data
