'use strict'

app = angular.module('taxiClientApp')

app.service 'BookingsSyncService', ($rootScope, $http) ->
  lastBookingId = 0
  save: (bookingDetails, callback) ->
    $http.post('http://localhost:3000/bookings', {booking: bookingDetails})
      .success (data, status) ->
        lastBookingId = data.bookingId
        callback( data.message )
  getLastBookingId: ->
    lastBookingId


# Asynchronous communication (via Pusher)
app.service 'BookingsPusherService', ($rootScope) ->
  pusher = new Pusher('aee2891b234084cfc01e')
  channel = pusher.subscribe('taxiClient')
  onMessage: (callback) ->
    channel.bind 'asyncNotification', (data) ->
      $rootScope.$apply ->
        callback(data.message)
