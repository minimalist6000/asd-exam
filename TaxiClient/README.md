# Setting up the project

Of course, you should start by installing all the required libraries and node packages. Use the following commands:

```sh
bower install
npm install
```

> NOTE that the project does not include the support to asynchronous interaction with the backend. You should 
> complete this part of the code (hint: you can copy the code from your own project or even from TaxiHome in this 
> project skeleton).

